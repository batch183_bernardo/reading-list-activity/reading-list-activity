

// Function return statement

// function returnInfo(firstName, lastName, age, city, province)   {
// 	return "Hello! I am "+ firstName +' '+lastName+', '+age+ " years old, and currently living in " +city+' '+province+'.';
// }

// let userInfo = {
// 	firstName: "John Carlo",
// 	lastName: "Bernardo",
// 	age: 25,
// 	currentAddress: {
// 		city: "Pasay City",
// 		province: "Metro Manila"
// 	}
// };

// let person = returnInfo(userInfo.firstName, userInfo.lastName, userInfo.age, userInfo.currentAddress.city, userInfo.currentAddress.province)
// console.log(person);

// If else statement

// let age = 17;
// if (age >= 18) {
// 	console.log("You're qualified to Vote!");
// } else {
// 	console.log("Sorry, You're too young to Vote.");
// }

// Switch prompt alert Statemet

// let month = prompt('Enter month number 1-12');
// switch (month) {
// 	case '1':
// 		console.log('Total Number of days for January: 31')
// 		break
// 	case '2':
// 		console.log('Total Number of days for February: 28')
// 		break
// 	case '3':
// 		console.log('Total Number of days for March: 30')
// 		break
// 	case '4':
// 		console.log('Total Number of days for April: 30')
// 		break	
// 	case '5':
// 		console.log('Total Number of days for May: 30')
// 		break
// 	case '6':
// 		console.log('Total Number of days for June: 30')
// 		break
// 	case '7':
// 		console.log('Total Number of days for July: 31')
// 		break
// 	case '8':
// 		console.log('Total Number of days for August: 31')
// 		break
// 	case '9':
// 		console.log('Total Number of days for September: 30')
// 		break
// 	case '10':
// 		console.log('Total Number of days for October: 31')
// 		break
// 	case '11':
// 		console.log('Total Number of days for November: 30')
// 		break
// 	case '12':
// 		console.log('Total Number of days for December: 31')
// 		break
// 	default:
// 		alert("Invalid input! Please enter the month number between 1-12");
// }

// Leap Year

// let leap = function (year){
// 	if (year % 4 ==0 || year % 400 ==0){
// 		return  "Is a leap year"
// 	} else {
// 		return "Not a leap year!"
// 	}
// };

// let result = leap (prompt("Enter year"));
// console.log(result);


// Looping Statement



for (let i = (prompt("Enter a number")); i >= 0; i--) {
	console.log(i);
	};


